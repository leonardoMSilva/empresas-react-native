import React, { Component } from 'react';
import { ScrollView, View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import Styles from './styles';

class ProfileEnterprise extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterprise: [],
      type: ''
    }
  }

  async componentDidMount() {
    let config = {
      headers: {
        'Content-Type': 'application/json',
        'access-token': this.props.headers.acessToken,
        'client': this.props.headers.client,
        'uid': this.props.headers.uid
      }
    }

    const response = await axios.get('http://empresas.ioasys.com.br/api/v1/enterprises/' + this.props.profileEnterprise.id, config);

    if (response.status == 200) {
      this.setState({ enterprise: response.data.enterprise });
      this.setState({ type: response.data.enterprise.enterprise_type.enterprise_type_name });
    }

  }

  render() {

    const { enterprise } = this.state;

    return (
      <ScrollView>
        <View style={Styles.card}>
          <View style={Styles.viewOne}>
            {enterprise.photo != null || enterprise.photo != "" ?
              <Image
                style={Styles.img}
                source={{ uri: 'http://empresas.ioasys.com.br' + enterprise.photo }}
              />
              :
              <Image
                style={Styles.img}
                source={{ uri: 'http://hcseguro.com.br/wp-content/uploads/2017/07/empresa.png' }}
              />}
          </View>
          <View style={Styles.viewTwo}>
            <Text style={Styles.titlePrice}>Preço:</Text>
            <Text style={Styles.price}>{enterprise.share_price}</Text>
          </View>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Descrição</Text>
          <Text style={Styles.description}>{enterprise.description}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Nome da empresa:</Text>
          <Text style={Styles.itens}>{enterprise.enterprise_name}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Localização</Text>
          <Text style={Styles.itens}>{enterprise.city}, {enterprise.country}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Tipo da empresa</Text>
          <Text style={Styles.itens}>{this.state.type}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Empresa própria</Text>
          {enterprise.own_enterprise == true ? <Text style={Styles.itens}>Sim</Text>
            : <Text style={Styles.itens}>Não</Text>}
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>E-mail</Text>
          <Text style={Styles.itens}>{enterprise.email_enterprise}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Facebook</Text>
          <Text style={Styles.itens}>{enterprise.facebook}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Twitter</Text>
          <Text style={Styles.itens}>{enterprise.twitter}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>LinkedIn</Text>
          <Text style={Styles.itens}>{enterprise.linkedin}</Text>
        </View>
        <View style={Styles.cards}>
          <Text style={Styles.title}>Telefone</Text>
          <Text style={Styles.itens}>{enterprise.phone}</Text>
        </View>
      </ScrollView>
    );
  }
}

export default connect(state => ({ profileEnterprise: state.profileEnterprise, headers: state.auth }))(ProfileEnterprise)