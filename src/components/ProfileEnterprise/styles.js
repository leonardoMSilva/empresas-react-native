import { StyleSheet } from 'react-native';

export default StyleSheet.create({

    card: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: 'grey',
        padding: 20,
    },
    viewOne: {
        width: '60%',
    },
    img: {
        width: 130,
        height: 130,
        borderRadius: 50,
    },
    viewTwo: {
        justifyContent: 'center',
        alignContent: 'center',
        width: '40%',
    },
    titlePrice: {
        fontSize: 20,
    },
    price: {
        fontSize: 22,
        color: 'black'
    },
    cards: {
        borderBottomWidth: 1,
        borderColor: 'grey',
        padding: 20,
    },
    title: {
        fontSize: 18,
    },
    itens: {
        fontSize: 20,
        color: 'black',
    },
    description: {
        fontSize: 15,
        color: 'black',
    }
});