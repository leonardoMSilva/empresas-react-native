import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    
    container: {
        backgroundColor: 'black',
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input:{
        backgroundColor: 'white',
        width: '70%',
        margin: 5,
        textAlign: 'center',
        height: 50,
    },
    button:{
        backgroundColor: '#007bff',
        width: '70%',
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white',
        height: 50,
    },
    tWhite:{
        color: 'white',
    }
});