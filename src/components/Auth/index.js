import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Text, Image } from 'react-native';
import { connect } from 'react-redux';

import axios from 'react-native-axios';
import Styles from './styles';
import * as AuthActions from '../../Store/actions/auth';
import logo from '../../logo.png';


class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  Authentication = async (e) => {
    const auth = {
      email: this.state.email, 
      password: this.state.password,
    }

    const response = await axios.post('http://empresas.ioasys.com.br/api/v1/users/auth/sign_in', auth);


    if (response.status == 200) {
      this.props.dispatch(AuthActions.auth(response.headers.map["access-token"], response.headers.map.client, response.headers.map.uid));
      this.props.navigation.navigate("Enterprises");
    } else {
      alert('Usuário ou senha inválidos!');
    }

  }

  render() {
    return (
      <View style={Styles.container}>
        <Image
          source={logo}
        />

        <TextInput
          placeholder="Digite o seu e-mail"
          onChangeText={(text) => this.setState({ email: text })}
          style={Styles.input}
        />
        <TextInput
          placeholder="Digite a sua senha"
          onChangeText={(text) => this.setState({ password: text })}
          secureTextEntry={true}
          style={Styles.input}
        />

        <TouchableOpacity style={Styles.button} onPress={() => this.Authentication()}>
          <Text style={Styles.tWhite}>Login</Text>
        </TouchableOpacity>
      </View>);
  }
}

export default connect(state => ({ auth: state.auth }))(Login)