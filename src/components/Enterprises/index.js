import React, { Component } from 'react';
import { ScrollView, Text, Image, View, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import axios from 'react-native-axios';

import Styles from './styles';
import { search } from '../../Store/actions/search';
import { profileEnterprise } from '../../Store/actions/profileEnterprise';
import enterpriseType from '../../EnterpriseType.json';

class Enterprises extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterprises: [],
      enterpriseType: enterpriseType.types,
      text: '',
      loading: true
    }
  }

  async componentDidMount() {
    let config = {
      headers: {
        'Content-Type': 'application/json',
        'access-token': this.props.modules.acessToken,
        'client': this.props.modules.client,
        'uid': this.props.modules.uid
      }
    }

    const response = await axios.get('http://empresas.ioasys.com.br/api/v1/enterprises', config);

    if (response.status == 200) {
      this.setState({ enterprises: response.data.enterprises, loading: false });
    }
  }

  detailEmp = (id) => {
    this.props.dispatch(profileEnterprise(id));

    this.props.navigation.navigate("ProfileEnterprise");
  }

  selected = (e) => {
    let aux = 0;

    this.state.enterpriseType.forEach((e) => {
      if (this.state.text == e.type) {
        this.props.dispatch(search(e.number, "type"));
        aux = 1;
        this.props.navigation.navigate("ResultSearch");
      }
    })

    if (aux == 0) {
      this.props.dispatch(search(this.state.text, "enterprise"));
      this.props.navigation.navigate("ResultSearch");
    }
  }

  render() {
    if (this.state.loading == true) {
      return (
        <View style={{ alignSelf: 'center' }}>
          <Text>Carregando...</Text>
        </View>
      );
    }

    return (
      <ScrollView>
        <View style={Styles.searchBar}>
          <TextInput
            placeholder="Digite o nome ou a área da empresa"
            style={Styles.textSearchBar}
            onSubmitEditing={(e) => this.selected(e)}
            onChangeText={(text) => this.setState({ text })}
          />
        </View>
        {this.state.enterprises.map((enterprise) =>
          <View key={enterprise.id}>
            <View style={Styles.card}>
              <TouchableOpacity onPress={() => this.detailEmp(enterprise.id)} style={Styles.touchCard}>
                <View style={Styles.viewOne}>
                  {enterprise.photo != null ?
                    <Image
                      style={Styles.img}
                      source={{ uri: 'http://empresas.ioasys.com.br' + enterprise.photo }}
                    />
                    :
                    <Image
                      style={Styles.img}
                      source={{ uri: 'http://hcseguro.com.br/wp-content/uploads/2017/07/empresa.png' }}
                    />
                  }
                </View>
                <View style={Styles.viewTwo}>
                  <Text>
                    Empresa: {enterprise.enterprise_name}
                  </Text>
                  <Text>
                    Cidade: {enterprise.city}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </ScrollView>
    );
  }
}

export default connect(state => ({ modules: state.auth }))(Enterprises)
