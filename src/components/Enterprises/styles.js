import { StyleSheet } from 'react-native';

export default StyleSheet.create({

    searchBar: {
        backgroundColor: 'black',
        paddingBottom: 15,
        paddingLeft: 15,
    },
    textSearchBar: {
        backgroundColor: 'white',
        width: '95%',
        borderRadius: 5,
    },
    card: {
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: '#E6E6E6',
    },
    touchCard: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 10,
        paddingTop: 10,
        backgroundColor: 'white',
    },
    viewOne: {
        width: '22%',
        alignItems: 'center',
    },
    viewTwo: {
        width: '60%'
    },
    img: {
        width: 50,
        height: 50,
        borderRadius: 50
    }
});