import React, { Component } from 'react';
import { ScrollView, Text, Image, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import axios from 'react-native-axios';

import { profileEnterprise } from '../../Store/actions/profileEnterprise';
import Styles from '../Enterprises/styles';


class ResultSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            enterprises: [],
        }
    }

    async componentDidMount() {
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'access-token': this.props.headers.acessToken,
                'client': this.props.headers.client,
                'uid': this.props.headers.uid
            }
        }
        let baseUrl

        if (this.props.enterprise.option == 'type') {
            baseUrl = `http://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=` + this.props.enterprise.text;
        } else {
            baseUrl = `http://empresas.ioasys.com.br/api/v1/enterprises?name=` + this.props.enterprise.text;
        }

        const response = await axios.get(baseUrl, config)

        if (response.status == 200) {
            await this.setState({ enterprises: response.data.enterprises });
        }
    }

    detailEmp = (id) => {
        this.props.dispatch(profileEnterprise(id));

        this.props.navigation.navigate("ProfileEnterprise");
    }

    render() {
        return (
            <ScrollView>
                {this.state.enterprises.map((enterprise) =>
                    <View key={enterprise.id}>
                        <View style={Styles.card}>
                            <TouchableOpacity onPress={() => this.detailEmp(enterprise.id)} style={Styles.touchCard}>
                                <View style={Styles.viewOne}>
                                    {enterprise.photo != null ?
                                        <Image
                                            style={Styles.img}
                                            source={{ uri: 'http://empresas.ioasys.com.br' + enterprise.photo }}
                                        />
                                        :
                                        <Image
                                            style={Styles.img}
                                            source={{ uri: 'http://hcseguro.com.br/wp-content/uploads/2017/07/empresa.png' }}
                                        />
                                    }
                                </View>
                                <View style={Styles.viewTwo}>
                                    <Text>Empresa: {enterprise.enterprise_name}</Text>
                                    <Text>Cidade: {enterprise.city}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            </ScrollView>
        );
    }
}

export default connect(state => ({ enterprise: state.search, headers: state.auth }))(ResultSearch)
