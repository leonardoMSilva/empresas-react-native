const INITIAL_STATE = {
    text: '',
    option: ''
}

function search(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'SEARCH_ENTERPRISE':
            state = {
                text: action.text,
                option: action.option,
            };
            break;
    }
    return state;
}

export default search
