import { combineReducers } from 'redux'

import auth from './auth'
import search from './search'
import profileEnterprise from './profileEnterprise'

export default combineReducers({
    auth,
    search,
    profileEnterprise,
})