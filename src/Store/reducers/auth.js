const INITIAL_STATE = {
    acessToken: '',
    client: '',
    uid: 'aa',
}

function auth(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'AUTH_LOGIN':
            state = {
                acessToken: action.acessToken,
                client: action.client,
                uid: action.uid
            }
            break;
    }
    return state;
}

export default auth
