const INITIAL_STATE = {
    id: '',
}

function profileEnterprise(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'PROFILE_ENTERPRISE':
            state = {
                id: action.id,
            };
            break;
    }
    return state;
}

export default profileEnterprise
