export function auth(acessToken, client, uid) {
    return {
        type: 'AUTH_LOGIN',
        acessToken: acessToken,
        client: client,
        uid: uid
        
    }
}
