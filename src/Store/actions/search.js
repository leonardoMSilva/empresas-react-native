export function search(text, option) {
    return {
        type: 'SEARCH_ENTERPRISE',
        text: text,
        option: option,
    }
}
