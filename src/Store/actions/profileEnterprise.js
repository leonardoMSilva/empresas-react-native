export function profileEnterprise(id) {
    return {
        type: 'PROFILE_ENTERPRISE',
        id: id,
    }
}
