import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';

import { RootStack } from './config/Routes'

const AppContainer = createAppContainer(RootStack);

import store from './Store'


export default class App extends Component {
  render() {
    return <Provider store={store}><AppContainer /></Provider>
  }
}
