import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';


import Login from '../components/Auth';
import Enterprises from '../components/Enterprises'
import ResultSearch from '../components/Search'
import ProfileEnterprise from '../components/ProfileEnterprise'


const AppStack = createStackNavigator({
    Enterprises: Enterprises,
    ResultSearch: ResultSearch,
    ProfileEnterprise: ProfileEnterprise
}, {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: 'black',
                elevation: 0,
            },
            title: 'AppEmpresas',
            headerTintColor: 'white',
            headerTitleStyle: {
                color: 'white'
            },
        }
    }

);

export const RootStack = createAppContainer(createSwitchNavigator(
    {
        Login: Login,
        App: AppStack,
    },
    {
        initialRouteName: 'Login',
    }
));