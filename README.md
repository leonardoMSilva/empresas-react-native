
# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações necessárias para a avaliação do projeto AppEmpresas.

---

### Instalação ###
```sh
yarn install #ou npm install
react-native link react-native-gesture-handler
react-native run-android
```
---
### Libs utilizadas ###
* `redux/react-redux:` Neste projeto utilizei o redux para a alteração de estados. Após a autenticação os headers ficam salvos em um estado do redux.

* `react-navigation:` Realizar a navegação entre telas, utilizei o SwitchNavigation para o login e StackNavigation para as telas internas.

* `react-native-gesture-handler:` Lib que acompanha o react-navigation para lidar com gestos e toques no React-Native.

* `react-native-axios:` Utilizei essa lib para realizar as requisições para a API Empresas.


---

### Objetivo ###

* Desenvolver uma aplicação React Native que consuma a API `Empresas`, cujo Postman esta compartilhado neste repositório (collection).
* Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, **NÃO** é necessário criar um Pull Request para isso.
* Nós iremos realizar a avaliação e te retornar um email com o resultado.




